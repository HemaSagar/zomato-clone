import React from "react";

class CartItem extends React.Component {
    constructor(props){
        super(props);
    }
    delete = () => {
        this.props.onDelete(this.props.item.id);
    }

    increment = () => {
        this.props.onIncrement(this.props.item.id);
    }
    decrement =() =>{
        this.props.onDecrement(this.props.item.id,this.props.quantity);
    }
    render() {
        
        return (
            <div className="card mb-3 col-12 col-sm-10  cart-item-div" style={{maxHeight:"140px"}}>
                <div className="row g-0">
                    <div className="col-3 col-sm-4 ">
                        <img src={this.props.item.imgUrl}  className="img-fluid rounded-start" style={{height:"138px",width:"200px"}}  alt="..."/>
                        
                    </div>
                    <div className="col-9 col-sm-8">
                        <div className="card-body">
                            <h5 className="card-title ">{this.props.item.title}</h5>
                            <table className="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td className="d-flex gap-1">
                                            Quantity<div className="d-flex justify-content-center gap-2" >
                                            <button className="cart-change-buttons decrement" onClick={()=>{this.decrement()}}>-</button>
                                            <button className="cart-change-buttons increment" onClick={()=>{this.increment()}}>+</button>
                                            </div>
                                        </td>
                                        <td className="text-end">{this.props.quantity}</td>
                                    </tr>
                                    <tr>
                                        <td>Price</td>
                                        <td className="text-end">₹ {this.props.price}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CartItem;