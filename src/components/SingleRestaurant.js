import React, { Fragment } from "react";
import Header2 from "./Header2";
import RestaurantsData from "../restaurants";
import RestaurantItem from "./RestaurantItems";

class SingleRestaurant extends React.Component {
  render() {
    const currIndex = RestaurantsData.findIndex((restaurant) => {
      return restaurant.id === this.props.id;
    });
    const restaurant = RestaurantsData[currIndex];

    return (
      <Fragment>
        <Header2
          setUserName={this.props.setUserName}
          userName={this.props.userName}
        />
        <hr className="mt-0" />
        <section className="restaurant-container container">
          <div
            className="restaurant-images  d-flex gap-1"
            style={{ maxHeight: "350px", boxSizing: "border-box" }}
          >
            <img
              className="w-75 p-1"
              style={{ objectFit: "cover" }}
              src={restaurant.imgUrl}

              alt="restaurant"
            />
            <div className="d-flex flex-column w-25 mh-100  ">
              <img
                className="w-100 h-50 p-1 "
                style={{ objectFit: "cover" }}
                src="https://media.istockphoto.com/id/1159992039/photo/cozy-restaurant-for-gathering-with-friends.jpg?s=612x612&w=0&k=20&c=FTnWMb2J9lI7M6Q06xlCDBwDq291PbNeU_YwcnzH9f0="
                alt="restaurant"
              />
              <img
                className="w-100 h-50 p-1"
                style={{ objectFit: "cover" }}
                src="https://static4.depositphotos.com/1015060/494/i/600/depositphotos_4947647-stock-photo-restaurant.jpg"
                alt="restaurant"
              />
            </div>
          </div>
          <div className="d-flex justify-content-between mt-2 " style={{ maxHeight: "300px" }}>
            <div>
              <h1 className="">{restaurant.title}</h1>
              <p className="lead">{restaurant.category[0]}</p>
              <p className=" lead text-muted">{restaurant.location}</p>
            </div>
            <div
              className="rounded m-2"
              style={{
                background: "rgb(58, 183, 87)",
                height: "23px",
                width: "50px",
                color: "#fff",
              }}
            >
              <p className="text-center">{restaurant.rating}☆</p>            
            </div>
          </div>
          <hr />
          <div className="container our-dishes-container">
            <p className="fw-5 fs-4" style={{color:"rgb(239, 79, 95)"}}>Our Dishes</p>
            <div className="our-dishes">
            {restaurant.items.map((item)=> {
              return <RestaurantItem key={item.id} addToCart={this.props.addToCart} cartItems={this.props.cartItems} dish={item}/>
            })
            }
            </div>
          </div>

        </section>
      </Fragment>
    );
  }
}

export default SingleRestaurant;