import React from "react";
import Signup from "./Account/Signup";
import { Link } from "react-router-dom";

class Header2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
    };
  }
  changeUserName = (name) => {
    this.props.setUserName(name);
  };
  setSearch = (event) => {
    this.setState({
      search: event.target.value.trim(),
    });
  };
  render() {
    return (
      <>
        <div className="container md-container-fluid ">
          <header>
            <nav className="navbar row">
              <div className="row d-flex justify-content-between">
                <Link
                  to="/"
                  className="nav-item nav-link col-2 m-2 d-flex align-items-center"
                  
                >
                  <img
                    className="img-fluid"
                    src="https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png"
                    alt="logo"
                    style={{maxWidth:"100px"}}
                  />
                </Link>
                <div className="search-bar-wrapper col-12 col-md-6 m-2 order-2 order-md-1 border ">
                  <div className="container search-bar w-100">
                    <div className="location d-flex">
                      <i
                        className="sc-rbbb40-1 iFnyeo"
                        color="#FF7E8B"
                        size="20"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="#FF7E8B"
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          aria-labelledby="icon-svg-title- icon-svg-desc-"
                          role="img"
                          className="sc-rbbb40-0 iRDDBk"
                        >
                          <title>location-fill</title>
                          <path d="M10.2 0.42c-4.5 0-8.2 3.7-8.2 8.3 0 6.2 7.5 11.3 7.8 11.6 0.2 0.1 0.3 0.1 0.4 0.1s0.3 0 0.4-0.1c0.3-0.2 7.8-5.3 7.8-11.6 0.1-4.6-3.6-8.3-8.2-8.3zM10.2 11.42c-1.7 0-3-1.3-3-3s1.3-3 3-3c1.7 0 3 1.3 3 3s-1.3 3-3 3z"></path>
                        </svg>
                      </i>
                      <select
                        className="form-select "
                        aria-label="Default select example"
                      >
                        <option disabled selected>
                          Select location
                        </option>
                        <option>Marathalli</option>
                        <option>Koramangala</option>
                        <option>HSR Layout</option>
                      </select>
                    </div>
                    <div className="d-flex search-div align-items-center">
                      <input
                        type="text"
                        className="search-box"
                        placeholder="Search for restaurant, cuisine or a dish"
                        onChange={(event) => {
                          this.setSearch(event);
                        }}
                      />
                      {this.state.search.length > 0 ? (
                        <Link to={"/search/" + this.state.search}>
                          <div>
                            <i
                              className="sc-rbbb40-1 iFnyeo"
                              color="#828282"
                              size="18"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#828282"
                                width="18"
                                height="18"
                                viewBox="0 0 20 20"
                                aria-labelledby="icon-svg-title- icon-svg-desc-"
                                role="img"
                                className="sc-rbbb40-0 iwHbVQ"
                              >
                                <title>Search</title>
                                <path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path>
                              </svg>
                            </i>
                          </div>
                        </Link>
                      ) : (
                        <div>
                          <i
                            className="sc-rbbb40-1 iFnyeo"
                            color="#828282"
                            size="18"
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="#828282"
                              width="18"
                              height="18"
                              viewBox="0 0 20 20"
                              aria-labelledby="icon-svg-title- icon-svg-desc-"
                              role="img"
                              className="sc-rbbb40-0 iwHbVQ"
                            >
                              <title>Search</title>
                              <path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path>
                            </svg>
                          </i>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              

              
                <div
                  className="d-flex justify-content-end col-md-2 col-8 m-2  order-1 gap-4 align-items-center"
                  
                >
                  {this.props.userName.length > 0 ? (
                    <div className="d-flex">
                      <p className="m-auto"
                      >
                        <i className="fa-solid fa-user"></i> {this.props.userName}
                      </p>
                    </div>
                  ) : (
                    <div className="d-flex">
                      <button
                        href="#"
                        className="nav-item nav-link  nav-items"
                        style={{
                          background: "none",
                          border: "none",
                          color: "black",
                        }}
                        data-bs-toggle="modal"
                        data-bs-target="#SignUpModal"
                      >
                        Account
                      </button>
                    </div>
                  )}
                  <div className="cart nav-item nav-link  nav-items">
                    <Link
                      to="/cart"
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <i class="fa-solid fa-cart-shopping"></i>
                    </Link>
                  </div>
                </div>
              </div>
            </nav>
          </header>
          <Signup changeUserName={this.changeUserName} />
        </div>
      </>
    );
  }
}

export default Header2;