import React from "react";
import { Link } from "react-router-dom";

class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    addCart=(product)=>{
        this.props.addToCart(product);
        
    }
    render() {
        let cartQuantity = 0;
            let product = this.props.product;

            let currIndex = this.props.cartItems.findIndex((item) => {
                return Object.keys((item))[0] === this.props.product.id;
            })
            if (this.props.cartItems[currIndex]) {
                cartQuantity = this.props.cartItems[currIndex][product.id].quantity;
            }
        return (
            <Link to={'/restaurant/'+this.props.product.id}>
            <div className="card rounded-4 rounded-bottom  my-3 p-0" style={{ width: "19rem" }}>
                <img className="card-img-top rounded-4" style={{ height: "12rem", objectFit: "cover" }} src={this.props.data.imgUrl} alt="item" />
                <div className="card-body">
                    <div className="d-flex justify-content-between align-items-center">
                        <h6 className="text-start">{this.props.data.title}</h6>
                        <div className="rounded" style={{background:"rgb(58, 183, 87)",height:"23px",width:"50px",color:"#fff"}}>
                            <p >{this.props.data.rating}☆</p>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between">
                        <span className="lead" style={{ fontSize: "15px" }}>{this.props.data.category[0]} </span>
                        <span>
                        </span>
                        <span className="lead" style={{ fontSize: "15px" }}><svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" fill="currentColor" class="bi bi-currency-rupee" viewBox="0 0 16 16">
                            <path d="M4 3.06h2.726c1.22 0 2.12.575 2.325 1.724H4v1.051h5.051C8.855 7.001 8 7.558 6.788 7.558H4v1.317L8.437 14h2.11L6.095 8.884h.855c2.316-.018 3.465-1.476 3.688-3.049H12V4.784h-1.345c-.08-.778-.357-1.335-.793-1.732H12V2H4v1.06Z" />
                        </svg>{this.props.data.price} for one</span>
                    </div>
                </div>
            </div>
            </Link>
        );
    }
}

export default Item;