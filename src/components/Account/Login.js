import React from "react";

class Login extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        

        return (
            <>
                {/* <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Launch demo modal
                </button> */}

                <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel" style={{color:"black"}}>Login</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <form>
                                <div className="row mb-3">
                                    <label for="inputEmail3" className="col-sm-12 col-form-label" style={{color:"black"}}>Email</label>
                                    <div className="col-sm-10">
                                        <input type="email" className="form-control" id="inputEmail3" onChange={this.emailValidate}/>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <label for="inputPassword3" className="col-sm-12 col-form-label" style={{color:"black"}}>Password</label>
                                    <div className="col-sm-10">
                                        <input type="password" className="form-control" id="inputPassword3" />
                                    </div>
                                </div>

                            </form>
                            <button type="submit" className="btn btn-primary m-auto">Sign in</button>

                        </div>
                        <div className="modal-footer ">
                            <p>Don't have account ?<a href="#">create account</a></p>
                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                        </div>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default Login;