import React from "react";
import validator from "validator";

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            email: "",
            password: "",
            confirPassword: "",
            checkbox: false,
            unameError: "",
            emailError: "",
            passwordError: "",
            confirmPassError: "",
            checkboxError: ""
        }
    }

    validateUserName = (value) => {
        if (value.trim().length === 0) {
            this.setState({ unameError: true });
        }
        else {
            this.setState({ unameError: false, userName: value });
        }
    }
    validateEmail = (value) => {
        if (validator.isEmail(value)) {
            this.setState({ emailError: false, email: value });
        }
        else {
            this.setState({ emailError: true});
        }
    }

    validatePassword = (value) => {
        if (validator.isStrongPassword(value)) {
            this.setState({ passwordError: false, password: value });
        }
        else {
            this.setState({ passwordError: true});
        }
    }

    validateConfirmPassword = (value) => {
        if (validator.equals(this.state.password, value)) {
            this.setState({ confirmPassError: false, confirPassword: value });
        }
        else {
            this.setState({ confirmPassError: true});
        }
    }

    validateCheckbox = (checked) => {
        if (checked) {
            this.setState({ checkboxError: false, checkbox: checked });
        }
        else {
            this.setState({ checkboxError: true});
        }
    }

    saveDetails = (event) => {
            this.props.changeUserName(this.state.userName);
    }

    render() {
        const validity = [this.state.unameError,this.state.emailError , this.state.passwordError , this.state.confirmPassError, this.state.checkboxError];
        return (
            <>
                <div className="modal fade" id="SignUpModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" style={{ color: "black",maxWidth:"400px" }}>
                        <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5 lead" id="exampleModalLabel">Sign up</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body d-flex flex-column align-items-center">
                                <form className="d-flex flex-column align-items-center" style={{ color: "black" }}>
                                    <div className="row mb-3 w-100">
                                        <label for="inputEmail3" className="col-sm-12 col-form-label lead" >Full name</label>
                                        <div className="col">
                                            <input type="text" className="form-control  shadow-none" placeholder="Full Name" id="inputEmail3" onChange={(event) => { this.validateUserName(event.target.value) }} />
                                            {this.state.unameError ? <div style={{ color: "red", fontSize: "12px" }}>Fullname must not be empty</div> : null}
                                        </div>
                                    </div>
                                    <div className="row mb-3 w-100">
                                        <label for="signup-email" className="col-sm-12 col-form-label lead">Email</label>
                                        <div className="col">
                                            <input type="email" className="form-control shadow-none" placeholder="Email" id="signup-email" onChange={(event) => { this.validateEmail(event.target.value) }} />
                                            {this.state.emailError ? <div style={{ color: "red", fontSize: "12px" }}>Please enter valid email</div> : null}

                                        </div>
                                    </div>
                                    <div className="row mb-3 w-100">
                                        <label for="signup-password" className="col-sm-12 col-form-label lead">Password</label>
                                        <div className="col  form-div">
                                            <input type="password" className="form-control shadow-none" placeholder="Password" id="signup-password" onChange={(event) => { this.validatePassword(event.target.value) }} />
                                            {this.state.passwordError ? <div style={{ color: "red", fontSize: "12px" }}>Password must contain a uppercase, a lowercase,a number, a special character and 8 or more characters</div> : null}
                                        </div>
                                    </div>
                                    <div className="row mb-3 w-100">
                                        <label for="confirm-password" className="col-sm-12 col-form-label lead">Confirm Password</label>
                                        <div className="col">
                                            <input type="password" className="form-control shadow-none" placeholder="Confirm password" id="confirm-password" onChange={(event) => { this.validateConfirmPassword(event.target.value) }} />
                                            {this.state.confirmPassError ? <div style={{ color: "red", fontSize: "12px" }}>Password doesn't match</div> : null}

                                        </div>
                                    </div>
                                    <div className="d-flex my">

                                        <input className="form-check-input shadow-none" type="checkbox" id="invalidCheck" onClick={(event) => { this.validateCheckbox(event.target.checked) }} />
                                        <label className="form-check-label mx-2" style={{ fontSize: "12px" }} for="invalidCheck">

                                            I agree to Zomato's Terms of Service, Privacy Policy and Content Policies
                                        </label>
                                    </div>
                                    {this.state.checkboxError ? <div className="mx-3" style={{ color: "red", fontSize: "12px", alignSelf: "flex-start" }}>Please check this box</div> : null}
                                </form>
                                {/* <button type="submit" className="btn btn-primary m-auto my-3" disabled={(validity.includes(true) || validity.includes(""))?true:false} style={{ alignSelf: "center", background: "rgb(239, 79, 95)", border: "rgb(239, 79, 95)" }} data-bs-dismiss="modal" onClick={(event) => { this.saveDetails(event) }} >Create account</button> */}
                                <div className="d-grid w-100 mt-3">
                                    <button className="btn btn-primary" type="button" disabled={(validity.includes(true) || validity.includes(""))?true:false} style={{ alignSelf: "center", background: "rgb(239, 79, 95)", border: "rgb(239, 79, 95)" }} data-bs-dismiss="modal" onClick={(event) => { this.saveDetails(event) }} >Create account</button>
                                </div>

                            </div>
                            {/* <div className="modal-footer "> */}
                            {/* <p>Have account already ?<a href="#">Login</a></p> */}
                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                            {/* </div> */}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Signup;