import React from "react";
import Item from "./item";
import Header2 from "./Header2";
import RestaurantsData from "../restaurants";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            searchValue:null
        }
    }
    changeUserName = (name) => {
        this.props.setUserName(name);
    }
    addCart = (product) => {
        this.props.addToCart(product);
    }
    renderSearchResults = (value) => {
        this.setState({
            searchValue:value
        })
    }

    render() {
        const searchValue = this.props.value;
        
        const matchingRestaurants = RestaurantsData.filter((restaurant)=>{
            return restaurant.category.includes(searchValue) || restaurant.title.includes(searchValue);
        })
        console.log("matches",matchingRestaurants);
        
        return (
            <>
                <Header2 setUserName={this.props.setUserName} userName={this.props.userName} renderSearchResults={this.renderSearchResults}/>

                <section className="container dining-type">
                    <div className="w-50">

                        <div className="w-50 d-flex align-items-center">
                            <div style={{ width: "4rem" }}>
                                <img className="" style={{ width: "30px", height: "100%" }} src="https://b.zmtcdn.com/data/o2_assets/c0bb85d3a6347b2ec070a8db694588261616149578.png?output-format=webp" />
                            </div>
                            <div>
                                <p className="h4">Delivery</p>
                            </div>
                        </div>
                    </div>
                </section>
                <hr />

                <div className="container search-results mt-5">
                {searchValue==='online_delivery' ? 
                    <>
                    <h2>Best Restaurants in South Bangalore</h2>
                    <div className="container items mt-5 gap-3 d-flex flex-wrap justify-content-between text-center " style={{minHeight:"80vh"}}>

                        {RestaurantsData.map((restaurant) => {
                            return <Item data={restaurant} key={restaurant.id} addToCart={this.props.addToCart} cartItems={this.props.cartItems} product={restaurant}/>
                        })}

                    </div>
                    </>
                    :matchingRestaurants.length>0 ? 
                    <>
                    <h2>{searchValue} delivery Restaurants in South Bangalore</h2>
                    <div className="container items mt-5 d-flex flex-wrap justify-content-start text-center" >
                        {matchingRestaurants.map((restaurant) => {
                            return <Item data={restaurant} key={restaurant.id} addToCart={this.props.addToCart} cartItems={this.props.cartItems} product={restaurant}/>
                        })}
                    </div>
                    </>
                    
                    :<h1 className="text-center" style={{minHeight:"45vh"}}>No restaurants or dishes found, try something else</h1>

                }
                </div>
                
                
                
            </>
        );
    }
}

export default Search;