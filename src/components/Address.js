import React from "react";
import AddressModal from "./Modals/AddressModal";

class Address extends React.Component {
  constructor(props) {
    super(props);
  }
  selectThisAddress =() => {
    this.props.selectThisAddress(this.props.address);
  }

  render() {
    return (
      <div className="container">
        <h5 className="mt-2">Delivery Address</h5>
        <div>
            

            {this.props.address ? 
          <div className="addresses row">
            
              <p className="lead fs-6 col-10">{this.props.address}</p>
              <button
              className="btn btn-primary col-2 p-1 "
              type="button"
              style={{
                background: "rgb(239, 79, 95)",
                border: "rgb(239, 79, 95)",
                maxHeight: "30px",
              }}
              onClick={(event)=>{
                this.selectThisAddress();
              }}
            >
              Use this
            </button>
          </div>
            
             : <p>
              No addresses
              </p>
            }
          <button
            className="btn btn-primary"
            type="button"
            style={{
              background: "rgb(239, 79, 95)",
              border: "rgb(239, 79, 95)",
            }}
            data-bs-toggle="modal"
            data-bs-target="#AddressModal"
          >
            + Add new address
          </button>
        </div>
        <AddressModal saveAddress={this.props.saveAddress} />
      </div>
    );
  }
}

export default Address;
