import React from "react";

class PaymentModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardName: "",
    };
  }
  updateName = (event) => {
    console.log("name");
    this.setState({
      cardName: event.target.value,
    });
  };
  sendUpdate = () => {
    console.log("sending update");
    this.props.onSubmit(this.state.cardName);
  };
  render() {
    return (
      <div
        className="modal fade"
        id="carddetails"
        tabindex="-1"
        aria-labelledby="entercarddetails"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-centered"
          style={{ color: "black", maxWidth: "400px" }}
        >
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5 lead" id="entercarddetails">
                Enter card details
              </h1>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body d-flex flex-column align-items-center">
              <form
                className="d-flex flex-column align-items-center"
                style={{ color: "black" }}
              >
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="card-number"
                    className="col-sm-12 col-form-label lead"
                  >
                    Card Number*
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control  shadow-none"
                      id="card-number"
                    />
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="name-on-card"
                    className="col-sm-12 col-form-label lead"
                  >
                    Name on Card
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control  shadow-none"
                      id="name-on-card"
                      onChange={(event) => {
                        this.updateName(event);
                      }}
                    />
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label htmlFor="expiry" className="col-6 col-form-label lead">
                    Expiry date*
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control  shadow-none"
                      id="expiry"
                    />
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label htmlFor="expiry" className="col-6 col-form-label lead">
                    CVV*
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control  shadow-none"
                      id="cvv"
                    />
                  </div>
                </div>
              </form>
              <div className="d-grid w-100 mt-3">
                <button
                  className="btn btn-primary"
                  type="button"
                  style={{
                    alignSelf: "center",
                    background: "rgb(239, 79, 95)",
                    border: "rgb(239, 79, 95)",
                  }}
                  data-bs-dismiss="modal"
                  onClick={() => {
                    this.sendUpdate();
                  }}
                >
                  Submit details
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentModal;

