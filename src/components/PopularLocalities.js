import React from "react";
import Locality from "./Locality";

class PopularLocalities extends React.Component{
    render(){
        return (
            <div className="container d-flex flex-column my-5">
                <h3 className=" display-6" >Popular localities in and around<span className=""> Bengaluru</span></h3>
                <div className="row my-5">
                    <Locality count={444} city={"Marathalli"} />
                    <Locality count={190} city={"Indiranagar"}/>
                    <Locality count={200} city={"WhiteField"}/>
                    <Locality count={300} city={"Jayanagar"}/>
                    <Locality count={250} city={"HSR"}/>
                    <Locality count={450} city={"Sarjapur"}/>
                    <Locality count={344} city={"JP Nagar"}/>

                </div>
            </div>
        );
    }
}

export default PopularLocalities;