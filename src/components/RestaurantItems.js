import React from "react";

class RestaurantItem extends React.Component {
  constructor(props) {
    super(props);
  }

  addCart = (dish) => {
    this.props.addToCart(dish);
  };
  render() {
    let cartQuantity = 0;
    let dish = this.props.dish;

    let currIndex = this.props.cartItems.findIndex((item) => {
      return Object.keys(item)[0] === this.props.dish.id;
    });
    if (this.props.cartItems[currIndex]) {
      cartQuantity = this.props.cartItems[currIndex][dish.id].quantity;
    }
    return (
      <div
        className="card mb-3 "
        style={{ maxWidth: "600px", minHeight: "50px" }}
      >
        <div className="row g-0">
          <div className="col-4 h-50">
            <img
              src={this.props.dish.imgUrl}
              style={{ objectFit: "cover" }}
              className="img-fluid  w-100"
              alt="..."
            />
          </div>
          <div className="col-8 ">
            <div className="card-body">
              <h5 className="card-title">{this.props.dish.title}</h5>
              <div
                className="rounded text-center"
                style={{
                  background: "rgb(58, 183, 87)",
                  height: "15px",
                  width: "40px",
                  color: "#fff",
                  fontSize:"12px",
                }}
              >
                <p>{this.props.dish.rating}☆</p>
              </div>
              <p className="card-text text-muted " style={{ fontSize: "14px" }}>
                {this.props.dish.description.slice(0, 70) + "..."}
              </p>
              <div className="d-flex justify-content-between">
                <button
                  className="btn  p-0 add-to-cart"
                  type="button"
                  onClick={() => {
                    this.addCart(this.props.dish);
                  }}
                >
                  {cartQuantity > 0 ? "Remove " : "Add to Cart"}
                </button>
                <p>₹ {this.props.dish.price}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RestaurantItem;