import React from "react";
import Header2 from "./Header2";

class OrderPlaced extends React.Component{
    render(){
        return (<>
            <Header2 setUserName={this.props.setUserName} userName={this.props.userName} />
            <hr />
            <div className="container d-flex flex-column align-items-center justify-content-center">
                <div className="w-50 h-50">
                <img className="img-fluid" src="https://entrackr.com/storage/2019/02/zomato.jpg"/>
                </div>
                <h2 className="mt-3">Order on the way !!</h2>
            </div>
            </>
            );
    }
}

export default OrderPlaced;