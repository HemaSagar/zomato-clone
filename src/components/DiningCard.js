import React from "react";
import { Link } from "react-router-dom";


class DiningCard extends React.Component {
    render() {
        let card = (
            <div className="card col-md-auto mx-2 px-0 my-2 dining-type-card" style={{color:"black"}} >
                <div className="card-img-div">
                    <img className="card-img-top img-fluid" src={this.props.img} alt="Cardimage" />
                </div>
                <div className="card-body">
                    <h3>{this.props.type}</h3>
                    <p >{this.props.description}</p>
                </div>
            </div>

        )
        if (this.props.type === "Order online") {
            card = <Link className="col-md-auto mx-2 px-0 " style={{ textDecoration: 'none'}} to={'/search/'+'online_delivery'}>{card}</Link>
        }
        return card;
    }
}

export default DiningCard;