import React from "react";
import { Link, Redirect } from "react-router-dom";
import Address from "./Address";
import Payment from "./Payment";
import CartItem from "./CartItem";

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: null,
      selectedAddress: null,
      showitems: false,
    };
  }

  saveAddress = (address) => {
    let addressString = `${address.fullName},\n${address.address}\n${address.city},${address.state} ${address.pincode}`;
    this.setState({
      address: addressString,
    });
  };

  selectThisAddress = (address) => {
    this.setState({
      selectedAddress: address,
    });
  };

  showItems = () => {
    this.setState({
      showitems: true,
    });
  };

  render() {
    // if (this.props.cartItems.length === 0) {
    //   <Redirect to="/" />;
    // }
    const bill = this.props.bill;
    return (
      <>
        <header>
          <nav className="navbar bg-light mt-2">
            <div className="container">
              <Link to="/" className="navbar-brand ">
                <img
                  className="m-auto"
                  src="https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png"
                  alt="Bootstrap"
                  width="100"
                  height="30"
                />
              </Link>
              <h3>Checkout</h3>
              <h4>
                <i className="fa-solid fa-user"></i> {this.props.userName}
              </h4>
            </div>
          </nav>
        </header>
        <div className="container d-flex flex-md-row flex-column " style={{minHeight:"65vh"}}>
          <div className="col-12 col-md-8  mt-2">
            <div className="address border m-1 p-2 ">
              <Address
                saveAddress={this.saveAddress}
                selectThisAddress={this.selectThisAddress}
                address={this.state.address}
              />
            </div>
            <div className="payment border  m-1 p-2">
              <Payment
                address={this.state.selectedAddress}
                showItems={this.showItems}
              />
            </div>
            <div className=" Items border mb-5  m-1 p-2">
              <div className=" ">
                <h5
                  className={this.state.showitems ? "mt-2" : "mt-2 text-muted"}
                >
                  Review Items
                </h5>

                <div
                  className={
                    this.state.showitems
                      ? "d-block review-items  m-1 p-2"
                      : "d-none review-items  m-1 p-2"
                  }
                >
                  {this.props.cartItems.map((item) => {
                    let key = Object.keys(item)[0];
                    let values = Object.values(item[key]);
                    return (
                      <CartItem
                        item={values[0]}
                        quantity={values[1]}
                        onIncrement={this.props.onIncrement}
                        onDecrement={this.props.onDecrement}
                        price={values[2]}
                        key={key}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="checkout border shadow rounded col-12 col-md-4 h-100  m-sm-2 bill-details">
            <h5 className="p-2 mb-0 lead fw-bold">Bill summary</h5>
            <hr className="mt-0" />
            <table className="table table-borderless">
              <tbody>
                <tr>
                  <th className="lead fs-6 " scope="row">
                    Quantity
                  </th>
                  <td>{bill.quantity}</td>
                </tr>
                <tr>
                  <th className="lead fs-6" scope="row">
                    Price
                  </th>
                  <td>₹ {bill.price}</td>
                </tr>
                <tr className="border-bottom border-dark">
                  <th className="lead fs-6" scope="row">
                    Delivery fee
                  </th>
                  <td>₹ 50.5</td>
                </tr>
                <tr>
                  <th scope="row">Grand Total</th>
                  <th>₹ {bill.price - 50.5}</th>
                </tr>
                <tr>
                  <td colSpan="2">
                    <Link to="/orderplaced">
                      <div
                        className={
                          this.state.showitems ? "d-grid w-100" : "d-none"
                        }
                      >
                        <button
                          className="btn btn-primary"
                          type="button"
                          style={{
                            background: "rgb(239, 79, 95)",
                            border: "rgb(239, 79, 95)",
                          }}
                        >
                          Place order
                        </button>
                      </div>
                    </Link>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}

export default Checkout;
