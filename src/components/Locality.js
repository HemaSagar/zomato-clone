import React from "react";

class Locality extends React.Component{
    render(){
        return (
            <div className=" col-md-3 col-sm-6 col-xs-12 w-60 my-3 mx-2 border p-3">
                <h4 className="lead cut-text">{this.props.city}</h4>
                <span className="lead">{this.props.count} places</span>
            </div>
            
        );
    }
}

export default Locality;