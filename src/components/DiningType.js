import React from "react";
import DiningCard from "./DiningCard";

class DiningType extends React.Component {
    render() {
        return (

            <div className="container d-flex justify-content-center mt-5 mb-5 dining-type">
                <div className="row">

                   <DiningCard type={"Order online"} description={"Stay home and order to your doorstep"} img={"https://b.zmtcdn.com/webFrontend/e5b8785c257af2a7f354f1addaf37e4e1647364814.jpeg?output-format=webp&fit=around|402:360&crop=402:360;*,*"} />
                    <DiningCard type={"Dining"} description={"View the cities favourite dining venue"} img={"https://b.zmtcdn.com/webFrontend/d026b357feb0d63c997549f6398da8cc1647364915.jpeg?output-format=webp&fit=around|402:360&crop=402:360;*,*"} />

                    <DiningCard type={"Night life and clubs"} description={"Explore cities nightlife outlets"} img={"https://b.zmtcdn.com/webFrontend/d9d80ef91cb552e3fdfadb3d4f4379761647365057.jpeg?output-format=webp&fit=around|402:360&crop=402:360;*,*"} />
                </div>
            </div>
        );
    }
}

export default DiningType;