import React, { Fragment } from "react";
import CartItem from "./CartItem";
import Header2 from "./Header2";
import { Link } from "react-router-dom";

class Cart extends React.Component {
  constructor(props) {
    super(props);
  }
  changeUserName = (name) => {
    this.props.setUserName(name);
  };
  render() {
    const bill = this.props.bill;
    return (
      <Fragment>
        <Header2
          setUserName={this.props.setUserName}
          userName={this.props.userName}
        />
        {this.props.cartItems.length === 0 ? (
          <div className=" text-center" style={{ minHeight: "55vh" }}>
            <h2>Oops..! No items in the cart</h2>
            <div>
                <img className="img-fluid" src="https://www.mentormyboard.com/assets/img/empty-cart.png" alt="cart" />
            </div>
          </div>
        ) : (
          <section
            className="container cart-section "
            style={{ minHeight: "55vh" }}
          >
            <p className="h2">Your order</p>
            <hr />
            <div className=" d-flex flex-column flex-sm-row cart-main-section">
              <div className="cart-items col-12 col-sm-8 mt-sm-2 d-flex flex-wrap">
                {this.props.cartItems.map((item) => {
                  let key = Object.keys(item)[0];
                  let values = Object.values(item[key]);
                  return (
                    <CartItem
                      item={values[0]}
                      quantity={values[1]}
                      onDelete={this.props.onDelete}
                      onIncrement={this.props.onIncrement}
                      onDecrement={this.props.onDecrement}
                      price={values[2]}
                      key={key}
                    />
                  );
                })}
              </div>

              <div className="checkout border rounded col-12 col-sm-4 h-100  m-sm-2 border-secondary bill-details">
                <h5 className="p-2 mb-0 lead fw-bold">Bill summary</h5>
                <hr className="mt-0" />
                <table className="table table-borderless">
                  <tbody>
                    <tr>
                      <th className="lead fs-6" scope="row">
                        Quantity
                      </th>
                      <td>{bill.quantity}</td>
                    </tr>
                    <tr>
                      <th className="lead fs-6" scope="row">
                        Price
                      </th>
                      <td>₹ {bill.price}</td>
                    </tr>
                    <tr className="border-bottom border-dark">
                      <th className="lead fs-6" scope="row">
                        Delivery fee
                      </th>
                      <td>₹ 50.5</td>
                    </tr>
                    <tr>
                      <th scope="row">Grand Total</th>
                      <th>₹ {bill.price - 50.5}</th>
                    </tr>
                    <tr>
                      <td colSpan="2">
                        <Link to="/checkout">
                          <div className="d-grid w-100">
                            <button
                              className="btn btn-primary"
                              type="button"
                              style={{
                                background: "rgb(239, 79, 95)",
                                border: "rgb(239, 79, 95)",
                              }}
                            >
                              Checkout
                            </button>
                          </div>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        )}
      </Fragment>
    );
  }
}

export default Cart;