import React from "react";
import CollectionCard from "./CollectionCard";

class Collections extends React.Component{
    render(){
        return (
            <div className="container collections w-100 h-100 mt-5 pt-5">
                <h1 className=" display-5">Collections</h1>
                <p>Explore curated lists of top restaurants, cafes, pubs, and bars in Bengaluru, based on trends</p>
                <div className="row collection-cards mt-5">
                    <CollectionCard title={"New in town"}  img={"https://b.zmtcdn.com/data/collections/1d1e838de3c921a6bdacb382a3d01c62_1667458986.jpg?output-format=webp"}/>
                    <CollectionCard title={"Romantic Dining"} img={"https://b.zmtcdn.com/data/collections/4c8e5746cdee9c932ebc99ae999520cf_1665640799.jpg?output-format=webp"}/>
                    <CollectionCard title={"Best Microbreweries"} img={"https://b.zmtcdn.com/data/collections/06ec1c3471d9d7f7047298eb9ef2de56_1665565737.jpg?output-format=webp"}/>
                    <CollectionCard title={"Picturesque cafes"} img={"https://b.zmtcdn.com/data/collections/b90996d69bfe8d8c7e36d3a56893df74_1657000323.jpg?output-format=webp"}/>

                </div>

            </div>
        );
    }
}

export default Collections