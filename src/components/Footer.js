import React from "react";

class Footer extends React.Component{
    render(){
        return (
            <footer className="mt-5 d-flex flex-column align-items-center" style={{background:"rgb(248, 248, 248)"}}>
            
                <div className="mt-4 text-center"  >
                    <img className="image-fluid w-50" src="https://b.zmtcdn.com/web_assets/b40b97e677bc7b2ca77c58c61db266fe1603954218.png?fit=around|198:42&crop=198:42;*,*"/>
                </div>
                <div className="text-center mt-4">
                    <p className="lead " style={{fontSize:"0.9rem"}}>By continuing past this page, you agree to our Terms of Service, Cookie Policy, Privacy Policy and Content Policies. All trademarks are properties of their respective owners. 2008-2022 © Zomato™ Ltd. All rights reserved.</p>
                </div>
            </footer>
        );
    }
}

export default Footer