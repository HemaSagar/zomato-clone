import React from "react";
import PaymentModal from "./Modals/PaymentModal";

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submit: false,
      cardName: "",
    };
  }
  onSubmit = (name) => {

    console.log("entered submit");
    this.setState({
      submit: true,
      cardName: name,
    });
  }

  
  render() {
    const address = this.props.address;
    return (
      <div className="container ">
        <h5 className={address ? "mt-2" : "mt-2 text-muted"}>Payment</h5>
        <div className={address ? "d-block" : "d-none"}>
          <p className="lead fs-6">Pay with Debit/Credit/ATM Cards</p>
          {!this.state.submit ? (
            <button
              className="btn btn-primary"
              type="button"
              data-bs-toggle="modal"
              data-bs-target="#carddetails"
              style={{
                alignSelf: "center",
                background: "rgb(239, 79, 95)",
                border: "rgb(239, 79, 95)",
              }}
            >
              Enter card details
            </button>
          ) : (
            <div>
              <div className="row">
                <p className="col-8">Name on Card:</p>
                <span className="col-4">{this.state.cardName}</span>
              </div>
              <button
                className="btn btn-primary"
                type="button"
                style={{
                  alignSelf: "center",
                  background: "rgb(239, 79, 95)",
                  border: "rgb(239, 79, 95)",
                }}
                onClick={()=>{
                    this.props.showItems();
                }}
              >
                Review items
              </button>
            </div>
          )}
          <PaymentModal onSubmit={this.onSubmit} />
        </div>
      </div>
    );
  }
}

export default Payment;
