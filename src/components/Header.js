import React from "react";
import Login from "./Account/Login";
import Signup from "./Account/Signup";
import { Link } from "react-router-dom";

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            search:""
        }

    }

    changeUserName = (name) => {
        this.props.setUserName(name);
    }
    setSearch = (event) => {
        this.setState({
            search:event.target.value.trim()
        })
    }
    render() {
        return <div className="conatiner header-image">
            <header className="navbar-wrapper">
                <nav className="navbar navbar-expand header-nav" >
                    <div className="collapse navbar-collapse collapsing-nav w-100" >
                        <a className="nav-item nav-link " href="#">Get the app</a>
                        <div className="navbar-nav nav-items" style={{ position:"relative",fontSize: "20px", fontWeight: "500" }}>
                            {/* {this.props.userName.length > 0 ? <div className="nav-item nav-link nav-items drop-down">
                                    <p style={{ fontSize: "20px", fontWeight: "500" }}><i class="fa-solid fa-user"></i> {this.props.userName}</p>
                            </div> : <div className="d-flex"><button className="nav-item nav-link  nav-items" style={{ background: "none", border: "none" }} data-bs-toggle="modal" data-bs-target="#exampleModal">Log in</button>
                                <button href="#" className="nav-item nav-link  nav-items" style={{ background: "none", border: "none" }} data-bs-toggle="modal" data-bs-target="#SignUpModal">Sign up</button></div>} */}

                                {this.props.userName.length > 0 ? <div className="nav-item nav-link nav-items drop-down">
                                    <p style={{ fontSize: "20px", fontWeight: "500" }}><i className="fa-solid fa-user"></i> {this.props.userName}</p></div> : <div className="d-flex">
                                <button href="#" className="nav-item nav-link  nav-items" style={{ background: "none", border: "none" }} data-bs-toggle="modal" data-bs-target="#SignUpModal">Account</button></div>}
                            <Link to="/cart" className="nav-item nav-link nav-items" style={{ textDecoration: "none" }}><i className="fa-solid fa-cart-shopping"></i></Link>

                        </div>
                    </div>
                </nav>
            </header>
            {/* <Login /> */}
            <Signup changeUserName={this.changeUserName} />
            <div className="container content-container">

                <div className="container-flid logo-container">
                    <img src="https://b.zmtcdn.com/web_assets/8313a97515fcb0447d2d77c276532a511583262271.png" className="img-fluid logo-img" alt="Responsive image" />
                    {/* <img src="https://b.zmtcdn.com/web_assets/8313a97515fcb0447d2d77c276532a511583262271.png" /> */}
                </div>
                <div className="welcome-container">
                    <p className="text-responsive display-5">Discover the best food & drinks in Bengaluru</p>
                </div>
                <div className="search-bar-wrapper">
                    <div className="container search-bar">
                        <div className="location d-flex">
                            <i className="sc-rbbb40-1 iFnyeo" color="#FF7E8B" size="20"><svg xmlns="http://www.w3.org/2000/svg" fill="#FF7E8B" width="20" height="20" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" className="sc-rbbb40-0 iRDDBk"><title>location-fill</title><path d="M10.2 0.42c-4.5 0-8.2 3.7-8.2 8.3 0 6.2 7.5 11.3 7.8 11.6 0.2 0.1 0.3 0.1 0.4 0.1s0.3 0 0.4-0.1c0.3-0.2 7.8-5.3 7.8-11.6 0.1-4.6-3.6-8.3-8.2-8.3zM10.2 11.42c-1.7 0-3-1.3-3-3s1.3-3 3-3c1.7 0 3 1.3 3 3s-1.3 3-3 3z"></path></svg></i>
                            <select className="form-select " aria-label="Default select example">
                                <option disabled selected>
                                    Select location
                                </option>
                                <option>
                                    Marathalli
                                </option>
                                <option>
                                    Koramangala
                                </option>
                                <option>
                                    HSR Layout
                                </option>
                            </select>
                        </div>
                        <div className="d-flex search-div align-items-center">
                            <input type="text" className="search-box" placeholder="Search for restaurant, cuisine or a dish" onChange={(event)=>{this.setSearch(event)}}/>
                            {this.state.search.length>0 ? <Link to={'/search/'+this.state.search}>
                            <div >
                                <i className="sc-rbbb40-1 iFnyeo" color="#828282" size="18"><svg xmlns="http://www.w3.org/2000/svg" fill="#828282" width="18" height="18" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" className="sc-rbbb40-0 iwHbVQ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg></i>
                            </div>
                            </Link>
                            :<div >
                                <i className="sc-rbbb40-1 iFnyeo" color="#828282" size="18"><svg xmlns="http://www.w3.org/2000/svg" fill="#828282" width="18" height="18" viewBox="0 0 20 20" aria-labelledby="icon-svg-title- icon-svg-desc-" role="img" className="sc-rbbb40-0 iwHbVQ"><title>Search</title><path d="M19.78 19.12l-3.88-3.9c1.28-1.6 2.080-3.6 2.080-5.8 0-5-3.98-9-8.98-9s-9 4-9 9c0 5 4 9 9 9 2.2 0 4.2-0.8 5.8-2.1l3.88 3.9c0.1 0.1 0.3 0.2 0.5 0.2s0.4-0.1 0.5-0.2c0.4-0.3 0.4-0.8 0.1-1.1zM1.5 9.42c0-4.1 3.4-7.5 7.5-7.5s7.48 3.4 7.48 7.5-3.38 7.5-7.48 7.5c-4.1 0-7.5-3.4-7.5-7.5z"></path></svg></i>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }


}

export default Header;