import React from 'react';
import './App.css';
import Header from './components/Header';
import DiningType from './components/DiningType';
import Collections from './components/Collections';
import PopularLocalities from './components/PopularLocalities';
import Search from './components/Search';
import Footer from './components/Footer';
import { Route } from 'react-router-dom';
import Cart from './components/Cart';
import SingleRestaurant from './components/SingleRestaurant';
import Checkout from './components/Checkout';
import OrderPlaced from './components/OrderPlaced';

class App extends React.Component {

  constructor(){
    super();
    this.state={
      userName:"",
      cartItems:[]
    }
  }
  setUserName = (name)=>{
    this.setState({userName:name});
  }

  addToCart = (product) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == product.id;
    })
    if (currIndex === -1) {
      let copyCart = [...this.state.cartItems, { [product.id]: { product: { ...product }, quantity: 1, price: product.price } }];
      this.setState({
        cartItems: copyCart
      })
    }
    else {
      let copyCart = [...this.state.cartItems];
      copyCart.splice(currIndex, 1);
      this.setState({ cartItems: copyCart})
    }
  }


  onIncrement = (itemId) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == itemId;
    })
    let copyCart = JSON.parse(JSON.stringify(this.state.cartItems));
    copyCart[currIndex][itemId].price = (parseFloat(copyCart[currIndex][itemId].price) + parseFloat(this.state.cartItems[currIndex][itemId].product.price)).toFixed(2);
    copyCart[currIndex][itemId].quantity = (copyCart[currIndex][itemId].quantity + 1);
    this.setState({ cartItems: copyCart });
  }

  onDecrement = (itemId,currQuantity) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == itemId;
    })
    let copyCart = JSON.parse(JSON.stringify(this.state.cartItems));

    if(currQuantity===1){
      copyCart.splice(currIndex, 1);
    this.setState({ cartItems: copyCart })
    }
    else{
    copyCart[currIndex][itemId].price = (copyCart[currIndex][itemId].price - this.state.cartItems[currIndex][itemId].product.price).toFixed(2)
    copyCart[currIndex][itemId].quantity = (copyCart[currIndex][itemId].quantity - 1);
    this.setState({ cartItems: copyCart });
    }
  }

  billingDetails = () => {
    return this.state.cartItems.reduce((accumlator,item)=>{
      let key = Object.keys(item)[0];
      accumlator.quantity += parseInt(item[key].quantity);
      accumlator.price += parseInt(item[key].price);
      return accumlator;
    },{quantity:0,price:0})
  
  }

  render() {
    const bill = this.billingDetails();
    return (
      <>
        <Route path='/' exact>
          <Header setUserName={this.setUserName} userName={this.state.userName}/>
          <DiningType />
          <Collections />
          <PopularLocalities />
          <Footer />
        </Route>

        <Route path='/search/:value' render={(props)=>(
          <>
          <Search value={props.match.params.value} setUserName={this.setUserName} userName={this.state.userName} addToCart={this.addToCart} cartItems={this.state.cartItems}/>
          <Footer />
          </>
        )} />

        <Route path='/restaurant/:id' render={(props) => (
          <>
          <SingleRestaurant id={props.match.params.id} setUserName={this.setUserName} userName={this.state.userName} addToCart={this.addToCart} cartItems={this.state.cartItems} />
          <Footer />
          </>
        )} />

        <Route path='/cart'>
        <>
          <Cart setUserName={this.setUserName} userName={this.state.userName} cartItems={this.state.cartItems}  onIncrement={this.onIncrement} onDecrement={this.onDecrement} bill={bill}/>
          <Footer />
          </>
        </Route>

        <Route path='/checkout'>
        <>
            <Checkout bill={bill} userName={this.state.userName} cartItems={this.state.cartItems} onIncrement={this.onIncrement} onDecrement={this.onDecrement}/>
          <Footer />
          </>

        </Route>

        <Route path='/orderplaced'>
            <OrderPlaced userName={this.state.userName} cartItems={this.state.cartItems} />
        </Route>
        </>
    );
  }
}

export default App;
